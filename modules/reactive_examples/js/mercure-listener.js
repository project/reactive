(function ($, Drupal, drupalSettings, once) {
  Drupal.behaviors.reactive_examples_new = {
    attach: function (context, settings) {
      once('mercure_sub_new', 'html', context).forEach( function () {
        // let list = $(drupalSettings.wrapper_class, context);
        let list = $('.reactive-content-new-nodes', context);
        let url = new URL(drupalSettings.mercure_server);
        url.searchParams.append('topic', drupalSettings.mercure_topic_new);

        const eventSource = new EventSource(url);

        eventSource.onmessage = (e) => {
          let xmlDoc = $.parseXML(e.data), // because we publish in XML.
              xml = $(xmlDoc);
          list.append('<li class="reactive-content-item">'
            + Drupal.t('A new [') + xml.find('type>target_id').text() + Drupal.t('] has been published: ')
            + '<strong>' + xml.find('title>value').text() + '</strong>' + '</li>');
        }
      })
    }
  },
  Drupal.behaviors.reactive_examples_update = {
      attach: function (context, settings) {
        once('mercure_sub_update', 'html', context).forEach( function () {
          // let list = $(drupalSettings.wrapper_class, context);
          let list = $('.reactive-content-updated-nodes', context);
          let url = new URL(drupalSettings.mercure_server);
          url.searchParams.append('topic', drupalSettings.mercure_topic_update);

          const eventSource = new EventSource(url);

          eventSource.onmessage = (e) => {
            let xmlDoc = $.parseXML(e.data), // because we publish in XML.
              xml = $(xmlDoc);
            list.append('<li class="reactive-content-item">'
              + Drupal.t('A [') + xml.find('type>target_id').text() + Drupal.t('] node has been updated by: ')
              + '<strong>' + xml.find('updatedBy').text() + '</strong>' + '</li>');
          }
        })
      }
  },
  Drupal.behaviors.reactive_examples_delete = {
      attach: function (context, settings) {
        once('mercure_sub_delete', 'html', context).forEach( function () {
          // let list = $(drupalSettings.wrapper_class, context);
          let list = $('.reactive-content-deleted-nodes', context);
          let url = new URL(drupalSettings.mercure_server);
          url.searchParams.append('topic', drupalSettings.mercure_topic_delete);

          const eventSource = new EventSource(url);

          eventSource.onmessage = (e) => {
            let xmlDoc = $.parseXML(e.data), // because we publish in XML.
              xml = $(xmlDoc);
            list.append('<li class="reactive-content-item">'
              + Drupal.t('A [') + xml.find('type>target_id').text() + Drupal.t('] node has been deleted by: ')
              + '<strong>' + xml.find('deletedBy').text() + '</strong>' + '</li>');
          }
        })
      }
    }
} (jQuery, Drupal, drupalSettings, once));
