<?php

namespace Drupal\reactive_examples\EventSubscriber;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\reactive\Event\NodeDeleteEvent;
use Drupal\reactive\Event\NodePreSaveEvent;
use Drupal\reactive\Event\NodeUpdateEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Mercure\Update;

/**
 * Reactive Examples event subscriber.
 */
final class ReactiveExamplesSubscriber implements EventSubscriberInterface {

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  private $serialization;

  /**
   * @var \Symfony\Component\Mercure\HubInterface
   */
  private HubInterface $hub;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(MessengerInterface $messenger, HubInterface $hub) {
    $this->messenger = $messenger;
    $this->serialization = \Drupal::service('serializer');
    $this->hub = $hub;
  }

  /**
   * Node pre save event example.
   *
   * @param \Drupal\reactive\Event\NodePreSaveEvent $event
   *   Event.
   */
  public function nodePreSave(NodePreSaveEvent $event) {
    $entity = $event->getEntity();

    $update = new Update(
      '/examples_new_nodes',
      $this->serialization->encode($entity, 'xml') // CAN BE JSON AS WELL IF YOU LIKE.
    );

    $this->hub->publish($update);

    $this->messenger->addStatus(__FUNCTION__ . ': published new content on mercure hub.');
  }

  /**
   * @throws \Exception
   */
  public function nodeUpdate(NodeUpdateEvent $event) {
    $who = $event->getByUser();
    $node = $event->getEntity();

    $xml = $this->serialization->encode($node, 'xml');
    $sxml = new \SimpleXMLElement($xml);
    $sxml->addChild('updatedBy', $who);

    $update = new Update(
    '/examples_update_nodes',
      $sxml->asXML()
    );

    $this->hub->publish($update);

    $this->messenger->addStatus(__FUNCTION__ . ': published new content on mercure hub.');
  }

  public function nodeDelete(NodeDeleteEvent $event) {
    $who = $event->getByUser();
    $node = $event->getEntity();

    $xml = $this->serialization->encode($node, 'xml');
    $sxml = new \SimpleXMLElement($xml);
    $sxml->addChild('deletedBy', $who);

    $update = new Update(
      '/examples_deleted_nodes',
      $sxml->asXML()
    );

    $this->hub->publish($update);

    $this->messenger->addStatus(__FUNCTION__ . ': published new content on mercure hub.');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      NodePreSaveEvent::NODE_PRESAVE => ['nodePreSave'],
      NodeUpdateEvent::NODE_UPDATE => ['nodeUpdate'],
      NodeDeleteEvent::NODE_DELETE => ['nodeDelete'],
    ];
  }

}
