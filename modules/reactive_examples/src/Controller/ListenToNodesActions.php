<?php

namespace Drupal\reactive_examples\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Reactive Examples routes.
 */
final class ListenToNodesActions extends ControllerBase {

  /**
   * Builds the response.
   */
  public function nodeListener() {
    return [
      '#theme' => 'reactive_node_listener',
      '#attached' => [
        'library' => ['reactive_examples/node_listener'],
        // Hope yall have a local mercure for now.
        'drupalSettings' => [
          'mercure_server' => 'http://localhost:3000/.well-known/mercure',
          'mercure_topic_new' => '/examples_new_nodes',
          'mercure_topic_update' => '/examples_update_nodes',
          'mercure_topic_delete' => '/examples_deleted_nodes',
        ],
      ],
    ];
  }

}
