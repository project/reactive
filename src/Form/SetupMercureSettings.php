<?php

namespace Drupal\reactive\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Reactive Core settings for this site.
 */
class SetupMercureSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'reactive_setup_mercure_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['reactive.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mercure host'),
      '#description' => $this->t('Please, provide the mercure host name, i.e: http://localhost:3000/.well-known/mercure'),
      '#default_value' => $this->config('reactive.settings')->get('mercure_host'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('reactive.settings')
      ->set('mercure_host', $form_state->getValue('host'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
