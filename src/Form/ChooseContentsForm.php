<?php

namespace Drupal\reactive\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Reactive Core settings for this site.
 */
class ChooseContentsForm extends ConfigFormBase {

  static array $entityDefinitions = [];
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    self::setDefinitions(
      $container
        ->get('entity_type.manager')
        ->getDefinitions());
    return new static(
      $container->get('config.factory'),
    );
  }

  /**
   * List of all entity definitions available.
   *
   * @param \Drupal\reactive\Form\EntityTypeInterface[] $definitions
   *  Definitions.
   */
  private static function setDefinitions(array $definitions) {
    if (!empty(self::$entityDefinitions)) {
      return;
    }
    $new_definitions = [];
    foreach ($definitions as $definition) {
      $new_definitions[$definition->getClass()] = $definition->getBundleLabel();
    }
    self::$entityDefinitions = $new_definitions;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'reactive_choose_contents';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['reactive.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['description'] = [
      '#type' => 'item',
      '#description' => $this->t('Select entities to receive updates'),
    ];
    $form['listen_bundles'] = [
      '#type' => 'checkboxes',
      '#default_value' => $this->config('reactive.settings')->get('listen_bundles'),
      '#options' => self::$entityDefinitions,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('reactive.settings')
      ->set('listen_bundles', $form_state->getValue('listen_bundles'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
