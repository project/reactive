<?php

namespace Drupal\reactive\Event;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Session\AccountInterface;

abstract class ContentEntityPreSaveEventBase extends Event implements ContentEntityPreSaveEventInterface {

  /**
   * @var int|string|null
   */
  private $id;

  /**
   * @var \Drupal\Core\Session\AccountInterface
   */
  private AccountInterface $account;

  private string $byUser;

  /**
   * @var \Drupal\Core\Entity\EntityInterface
   */
  private EntityInterface $entity;

  /**
   * @return \Drupal\Core\Entity\EntityInterface
   */
  public function getEntity(): EntityInterface {
    return $this->entity;
  }

  /**
   * @return string
   */
  public function getByUser(): string {
    return $this->byUser;
  }

  /**
   * @return int|string|null
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @return \Drupal\Core\Session\AccountInterface
   */
  public function getAccount(): AccountInterface {
    return $this->account;
  }

  public function __construct(EntityInterface $entity, AccountInterface $account) {
    $this->entity = $entity;
    $this->id = $entity->id();
    $this->account = $account;
    $this->byUser = $account->getAccountName();
  }

}
