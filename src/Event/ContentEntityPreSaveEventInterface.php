<?php

namespace Drupal\reactive\Event;

use Drupal\Core\Entity\EntityInterface;

interface ContentEntityPreSaveEventInterface {

  const NODE_PRESAVE = 'node_presave';
  const NODE_UPDATE = 'node_update';
  const NODE_DELETE = 'node_delete';

  /**
   * A getter that returns a EntityInterface
   *
   * @return EntityInterface
   */
  public function getEntity(): EntityInterface;

}
